import { Component, OnInit } from '@angular/core';
import { RestEndpointService } from '../rest-endpoint.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  constructor(private restEndpointService : RestEndpointService,
              private router : Router) 
  { }

  ngOnInit() 
  { 
    console.log("ngOnInit");
    this.restEndpointService.getGreeting().subscribe(data => {
      console.log(data);
    });
  }

  onClickEncryptButton(string: String)
  {
    console.log("onClickEncryptButton " + string);
  }

  onClickDecryptButton(string: String)
  {
    console.log("onClickDecryptButton " + string);
  }


}
