package com.marcelo.cryptography.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.marcelo.cryptography.logic.HelloWorld;
import com.marcelo.cryptography.logic.VigenereCipher;

@RestController
@CrossOrigin
@RequestMapping(value = "/vigenerecipher")
public class AppController {

	@RequestMapping(value = "/helloworld", method = { RequestMethod.GET, RequestMethod.POST })
	public HelloWorld helloWorld() {
		HelloWorld hw = new HelloWorld();
		hw.setGreeting("Hello World!");
		return hw;
	}
	
	@RequestMapping(value = "/encrypt", method = { RequestMethod.GET, RequestMethod.POST })
	public VigenereCipher encrypt() {
		VigenereCipher vc = new VigenereCipher();
		return vc;
	}
	
	@RequestMapping(value = "/decrypt", method = { RequestMethod.GET, RequestMethod.POST })
	public VigenereCipher decrypt() {
		VigenereCipher vc = new VigenereCipher();
		return vc;
	}
	
	@RequestMapping(value = "/encrypttest", method = { RequestMethod.GET, RequestMethod.POST })
	public VigenereCipher encrypttest() {
		VigenereCipher vc = new VigenereCipher();
		String text = "Cryptography";
		String key = "secret";
		vc.encrypt(text,key);
		return vc;
	}
	
	@RequestMapping(value = "/decrypttest", method = { RequestMethod.GET, RequestMethod.POST })
	public VigenereCipher decrypttest() {
		VigenereCipher vc = new VigenereCipher();
		String text = "VWAGYHZWDGMR";
		String key = "secret";
		vc.decrypt(text,key);
		return vc;
	}


}