package com.marcelo.cryptography.logic;

public class VigenereCipher {
	
	public final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private String encryptedText;
	private String decryptedText;
	private String key;
	
	public VigenereCipher(){
		this.encryptedText = "";
		this.decryptedText = "";
		this.key = "";
	}

	public String getEncryptedText() {
		return encryptedText;
	}

	public void setEncryptedText(String encryptedText) {
		this.encryptedText = encryptedText;
	}

	public String getDecryptedText() {
		return decryptedText;
	}

	public void setDecryptedText(String decryptedText) {
		this.decryptedText = decryptedText;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void encrypt(String decryptedText, String key)
	{
		this.decryptedText = decryptedText.toUpperCase();
		this.key = key.toUpperCase();
		int keyIndex = 0;
		for(int i=0; i<this.decryptedText.length(); i++){
			char decryptedTextChar = this.decryptedText.charAt(i);
			int decryptedTextCharIntValue = this.ALPHABET.indexOf(decryptedTextChar);
			char keyChar = this.key.charAt(keyIndex);
			int keyCharIntValue = this.ALPHABET.indexOf(keyChar);
			int index = Math.floorMod( (decryptedTextCharIntValue + keyCharIntValue) ,this.ALPHABET.length() );
			this.encryptedText = this.encryptedText + this.ALPHABET.charAt(index);
			keyIndex++;
			if(keyIndex == this.key.length()){
				keyIndex = 0;
			}
		}
	}
	
	public void decrypt(String encryptedText, String key)
	{
		this.encryptedText = encryptedText.toUpperCase();
		this.key = key.toUpperCase();
		int keyIndex = 0;
		for(int i=0; i<this.encryptedText.length(); i++){
			char encryptedTextChar = this.encryptedText.charAt(i);
			int encryptedTextCharIntValue = this.ALPHABET.indexOf(encryptedTextChar);
			char keyChar = this.key.charAt(keyIndex);
			int keyCharIntValue = this.ALPHABET.indexOf(keyChar);
			int index = Math.floorMod( (encryptedTextCharIntValue - keyCharIntValue) ,this.ALPHABET.length() );
			this.decryptedText = this.decryptedText + this.ALPHABET.charAt(index);
			keyIndex++;
			if(keyIndex == this.key.length()){
				keyIndex = 0;
			}
		}
	}

}
